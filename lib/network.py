#!/usr/bin/python3

#
# Copyright Theo Friberg 2016 under MIT
#
# Class representing a whole network of sigmoid neurons
#

import utils
from neuron import Neuron

class Network():

    def __init__(self, widths):
        
        """Generate a neural network of sigmoids with layers matching the given widths. Neurons will have neutral weights and be unbiased."""

        # The iterator is quite special because we want to use both the current and the previous value

        heights = [widths[0],]
        heights.extend(widths) # We duplicate the first value of the list of heights.
        self.cells = []
        
        i = 1 # We skip the first value of the list
        while i < len(heights):
            layer = []
            for x in range(heights[i]):
                layer.append(Neuron([1/heights[i-1]]*heights[i-1], 0)) # We create a neuron with heights[i-1] connections
            self.cells.append(layer)
            i += 1

    def runThrough(self, triggers):

        """Feed the values in triggers and run them through the network. Returns the activations of the output layer."""

        currentValues = triggers
        callSigmoid = lambda cell: cell.sigmoidNeuron(currentValues)
        for layer in self.cells:
            currentValues = list(map(callSigmoid, layer))
        return list(currentValues)

    def invalidate(self, neuron, layer):
        neuron.valid = False
        i = layer+1
        while i < len(self.cells):
            for cell in self.cells[i]:
                cell.valid = False
            i += 1

    def __repr__(self):

        """Return a textual representation of the network."""

        return  "\n(Key: I=input, *=hidden, O=output)\n\n"+"I"*len(self.cells[0])+"\n"+"\n".join(["*"*len(layer) for layer in self.cells[1:-1]])+"\nO\n"
        
