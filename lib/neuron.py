#!/usr/bin/python3

#
# Copyright Theo Friberg 2016 under MIT
#
# The Neuron class
#

import utils

class Neuron():

    valid = False
    prevInputs = []
    value = 0
    
    def eq(self, a, b):
        for x in zip(a, b):
           if x[0] != x[1]:
               return False
        return True
    
    """The Neuron class represents a sigmoid neuron."""
    
    def __init__(self, weights, bias):
        
        """Create a neuron weighting the input connections with the weights given and with an overall bias of bias."""
        
        self.weights = weights
        self.bias = bias

    def sigmoidNeuron(self, inputs):

        """Activation value of the sigmoid neuron."""

        if self.valid and self.eq(inputs, self.prevInputs) :
            return self.value
        else:
            self.valid = True
            self.prevInputs = inputs
            self.value = utils.sigma(sum(weight*trigger for weight, trigger in zip(self.weights, inputs))/len(inputs))# + self.bias)
            return self.value

    def __repr__(self):

        """Textual representation of the neuron."""

        weightsDescription = [str(weight) for weight in self.weights]
        return "Sigmoid neuron with the weights "+", ".join(self.weightsDescription[:-1])+" and "+self.weightsDescription[-1] + " and with an overall bias of "+str(self.bias)+"."
