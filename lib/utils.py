#!/usr/bin/python3

import math
import sys

# Copyright Theo Friberg 2016 under MIT
#
# Utility functions
#

def sigma(val):
    """Logistic sigma function."""
    try:
        return 1/(1 + math.exp(-val))
    except:
        return 1/(1 + sys.float_info.max)
