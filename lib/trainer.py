#!/usr/bin/python3

#
# Copyright Theo Friberg 2016 under MIT
#
# This file contains logic to train neural networks and extract datasets
#

import random

def getNum(string):

    """Get a number from a string regardless if it is a float or an int."""
    
    try:
        return int(string)
    except:
        return float(string)

def loadSet(path):

    """Return the dataset in a file."""

    result = []
    try:
        with open(path, "r") as f:
            for line in f:
                result.append([getNum(val.strip()) for val in line.split(",")])
        return result
    except IOError:
        print("Couldn't open file "+path+".")

def cost(right, wrong):
    return right-wrong

def train(network, rightDataPath, wrongDataPath, quiet, iterations=60, step=0.1):

    """Train the network using the two datasets specified in the files given. Return the network."""

    # Load the two sets

    rightSet = loadSet(rightDataPath)
    wrongSet = loadSet(wrongDataPath)

    if not quiet:
        print("This may take a while on cPython.", end="\r")

    for x in range(iterations):
        rightData = random.choice(rightSet)
        wrongData = random.choice(wrongSet)
        for layer in range(len(network.cells)):
            i = 0
            for cell in network.cells[layer]:
                for weight in range(len(cell.weights)):
                    cell.weights[weight] += step
                    network.invalidate(cell, layer)
                    a = network.runThrough(rightData)[0]
                    aw = network.runThrough(wrongData)[0]
                    cell.weights[weight] -= 2*step
                    network.invalidate(cell, layer)
                    b = network.runThrough(rightData)[0]
                    bw = network.runThrough(wrongData)[0]
                    if cost(a, aw) > cost(b, bw):
                        cell.weights[weight] += 3*step
                        network.invalidate(cell, layer)
                if not quiet:
                    print("Training " + str(int(100 * x / iterations))+"% layer "+str(layer+1)+" / " +str(len(network.cells)) + " cells done: "+str(int(100*i/len(network.cells[layer])))+"% ", end="\r")
                i += 1
                network.runThrough(rightData)
                cell.valid = True
    if not quiet:
        print("Done training network"+" "*(38 + len(network.cells)*2))
    return network
