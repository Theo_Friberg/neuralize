

from tkinter import *
from tkinter import ttk
root = Tk()
ttk.Label(root, text="Path to the right set").grid(column=0, row=0, padx=10, pady=10)
ttk.Label(root, text="Path to the wrong set").grid(column=1, row=0, padx=10, pady=10)
ttk.Entry(root).grid(column=0, row=1, padx=10, pady=10)
ttk.Entry(root).grid(column=1, row=1, padx=10, pady=10)
ttk.Label(root, text="Number of iterations").grid(columnspan=2, row=2, padx=10, pady=10)
ttk.Entry(root).grid(columnspan=2, row=3, padx=10, pady=10)
ttk.Button(text="Start").grid(padx=10, pady=10, columnspan=2)
ttk.Progressbar(root, length=320, orient="horizontal", mode="determinate").grid(padx=10, pady=10, columnspan=2)
root.mainloop()
